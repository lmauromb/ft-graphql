const fetch = require('node-fetch')
const util = require('util');
const {
  GraphQLSchema,
  GraphQLObjectType,
  GraphQLInt,
  GraphQLString,
  GraphQLList
} = require('graphql')

const HeadlineQueryType = new GraphQLObjectType({
  name: 'HeadlineQuery',
  description: '...',

  fields: () => ({
    queryString: {
      type: GraphQLString,
      resolve: res =>
        res["queryString"]
    },
    maxResults: {
      type: GraphQLInt,
      resolve: res =>
        res["resultContext"]["maxResults"]
    },
    offset: {
      type: GraphQLInt,
      resolve: res =>
        res["resultContext"]["offset"]
    }
  })
});

const HeadlineResultType = new GraphQLObjectType({
  name: 'HeadlineResult',
  description: '...',

  fields: () => ({
    id: {
      type: GraphQLString,
      resolve: res =>
        res["id"]
    },
    aspectSet: {
      type: GraphQLString,
      resolve: res =>
        res["aspectSet"]
    },
    title: {
      type: GraphQLString,
      resolve: res =>
        res["title"]["title"]
    },
    url: {
      type: GraphQLString,
      resolve: res =>
        res["location"]["uri"]
    },
    date: {
      type: GraphQLString,
      resolve: res =>
        res["lifecycle"]["lastPublishDateTime"]
    },
    summary: {
      type: GraphQLString,
      resolve: res =>
        res["summary"]["excerpt"]
    },
    editorial: {
      type: GraphQLString,
      resolve: res =>
        res["editorial"]["byline"]
    }
  })
});

const HeadlineType = new GraphQLObjectType({
  name: 'Headline',
  description: '...',

  fields: () => ({
    headlineQuery: {
      type: HeadlineQueryType,
      resolve: res => 
        res["query"]
    },
    results: {
      type: new GraphQLList(HeadlineResultType),
      resolve: res =>
        res["results"][0]["results"]
    }
  })
});

module.exports =  new GraphQLSchema({
  query: new GraphQLObjectType({
      name: 'Query',
      description: '...',

      fields: () => ({
          headline: {
              type: HeadlineType,
              args: {
                  queryString: { type: GraphQLString },
                  maxResults: { type: GraphQLInt },
                  offset: { type: GraphQLInt }
              },
              resolve: (root, args) => fetch(
                'https://api.ft.com/content/search/v1', {
                  method: 'POST',
                  headers: {
                    'X-Api-Key': '59cbaf20e3e06d3565778e7bf73b92c3e7e941c8b41a27ab5f55113c',
                    'Content-Type': 'application/json'
                  },
                  body: JSON.stringify({
                    "queryString": `${args.queryString}`,
                    "resultContext" : {
                       "aspects" :[  "title","lifecycle","location","summary","editorial" ],
                       "maxResults" : `${args.maxResults}`,
                       "offset": `${args.offset}`
                    }
                  })
                }
              ).then((response) => response.json())
          }
      })
  })
});