# FT GraphQL

## Overview

GraphQL server, for the FT Headline API, developed for the technical task of FT Technology Talent Scheme.

for details, check:
- [FT Headline API Reference](https://developer.ft.com/portal/docs-quick-start-guides-headline-licence)

The headline licence mostly uses the Search Endpoint
- [production](https://ft-search-grahpql.now.sh/api)

## Example

- In order to query you will need 3 arguments:
  - queryString
  - maxResults
  - offset

- *queryString* is what to search in the FT.
- *maxResults* and *offset* are used for pagination.

```JavaScript
query {
  headline(queryString: "", maxResults: 2, offset: 0) {
    results {
      id,
      aspectSet,
      title,
      summary,
      date
    }
  }
}
```

```JSON
{
  "data": {
    "headline": {
      "results": [
        {
          "id": "5ed3b600-2a03-11e8-9b4b-bc4b9f08f381",
          "aspectSet": "article",
          "title": "Putin eyes huge win as Russia goes to the polls",
          "summary": "Russians have begun voting in an election expected to confirm Vladimir Putin as president for another six years. Voting...",
          "date": "2018-03-17T20:02:41Z"
        },
        {
          "id": "2034da4e-2988-11e8-b27e-cc62a39d57a0",
          "aspectSet": "article",
          "title": "Facebook bans political data company Cambridge Analytica",
          "summary": "Facebook has banned Cambridge Analytica, the political data company that worked with the Trump presidential campaign...",
          "date": "2018-03-17T18:45:16Z"
        }
      ]
    }
  }
}
```

## Disclaimer

The server was developed (and used) only for the technical task, all the info is property of the **Financial Times**.