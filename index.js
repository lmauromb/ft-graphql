const graphqlHTTP = require('express-graphql');
const CORS = require('micro-cors')();

const schema = require('./schema');

const rootValue = "/api";

module.exports = CORS(graphqlHTTP({ 
  schema,
  rootValue,
  graphiql: false
}));

